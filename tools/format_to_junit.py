import argparse
import xml.etree.cElementTree as ET
import json
import copy

def format_json():
    test_suites = ET.Element("testsuites",name="Test Suite")
    test_suite = ET.SubElement(test_suites,"testsuite", name="SAST Suites")

    sast_file = open(args.file_paths)
    sast_json = json.load(sast_file)

    for rule in sast_json["vulnerabilities"]:

        test_case = ET.SubElement(test_suite, "testcase", name=rule["name"])
        failure = ET.SubElement(test_case, "failure")
        properties = ET.SubElement(test_case, "properties")
        property = ET.SubElement(properties, "property", name="property1")
        #failure.text = f"WARNING: Test\nCategory: 1\nFile: file\nLine: 1"

    return ET.ElementTree(test_suites)

def format_xmls():
    xml_paths = args.file_paths.split(" ")
    tree = ET.parse(xml_paths[0])
    original_root = tree.getroot()

    extended_tree = copy.deepcopy(tree)
    extended_root = extended_tree.getroot()

    xml_paths = xml_paths[1:]

    for path in xml_paths:
        temp_tree = ET.parse(path)
        temp_root = temp_tree.getroot()

        for temp_test_suite in temp_root:
            suite_count = 0
            added = False

            for test_suite in original_root:
                if extended_root[suite_count].attrib['name'] == temp_test_suite.attrib['name']:
                    extended_root[suite_count].extend(temp_test_suite)

                    for key in extended_root[suite_count].attrib:
                        if key != "name":
                            if key != "time":
                                extended_root[suite_count].attrib[key] = str(int(test_suite.attrib[key]) + int(temp_test_suite.attrib[key]))
                                added = True
                                break
                            if key == "time":
                                extended_root[suite_count].attrib[key] = str(float(test_suite.attrib[key]) + float(temp_test_suite.attrib[key]))
                                added = True
                                break  
                                
                suite_count += 1 
            
            if added == False:
                extended_root.extend(temp_root)

        original_root = copy.deepcopy(extended_root)

    return extended_tree

if __name__=="__main__":

    parser = argparse.ArgumentParser(description='Select the project, builds and suite')
    parser.add_argument('-f','--file_paths',required=True,help='Pathway to File')
    parser.add_argument('-ft','--file_type',required=True,help='The type of file to convert')
    args = parser.parse_args()
    
    tree = None

    if args.file_type == 'json':
        tree = format_json()

    elif args.file_type == 'xml':
        tree = format_xmls()

    if tree:
        tree.write("tests.xml")