This project was setup to test how to implement SAST or Semgrep. The project started with a focus on gitlab SAST but now is more setup for Semgrep. Viewing the commit history should show how it has changed

The file tools/format_to_junit.py is used to either convert xml files to junit xml, or SAST json files to junit xml in order to display test results in the gitlab pipeline UI. The folder tests/ contains some tests of converting xml to junit xml.

To run format_to_junit.py call the script then,

If converting SAST json to junit xml add in: -ft "json" -f path_to_file

If converting xml to junit xml add in: -ft "xml" -f "pathways to files separated by spaces"